package br.com.afi.mongodb;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Set;

import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

public class Main {

	public static void main(String[] args) throws UnknownHostException {
		MongoClient mongo = new MongoClient( "webhomolog" , 27017 );
		List<String> dbs = mongo.getDatabaseNames();
		
		System.out.println("DATABASES LIST");
		for(String dbname : dbs){
			System.out.println(dbname);
		}
		
		System.out.println("-------------------");
		System.out.println("COLLECTIONS LIST - HANDSON");
		DB db = mongo.getDB("handson");
		Set<String> tables = db.getCollectionNames();
		for(String coll : tables){
			System.out.println(coll);
		}
		
		System.out.println("-------------------");
		
		//insertUser(db);
		//insertUser2(db);
		//updateUser(db);
		//findUserByTelefone(db);
		//listUsers(db);
		//insertCupons(db);
		//aggregate(db);
	}
	
	private static void listUsers(DB db){
		System.out.println("LIST");
		
		DBCollection collection = db.getCollection("user");
		
		final DBCursor cursor = collection.find();
		
		for(DBObject dbObject : cursor){
			System.out.println(dbObject.toString());
		}	
	}
	
	private static void insertUser(DB db){
		System.out.println("INSERT");
		
		DBCollection collection = db.getCollection("user");
		BasicDBObject document = new BasicDBObject();
		document.put("_id", Integer.valueOf(1));
		document.put("name", "Andr�");
		document.put("age", 30);
		document.put("datInc", new Date());
		collection.insert(document);		
	}
	
	private static void insertUser2(DB db){
		System.out.println("INSERT 2");
		
		DBCollection collection = db.getCollection("user");
		
		BasicDBObject document = new BasicDBObject();
		BasicDBObject[] telefones = new BasicDBObject[2]; 
		
		telefones[0] = new BasicDBObject();
		telefones[0].put("ddd", 11);
		telefones[0].put("numero", "98765432");
		
		telefones[1] = new BasicDBObject();
		telefones[1].put("ddd", 11);
		telefones[1].put("numero", "12345678");
		
		document.put("_id", Integer.valueOf(2));
		document.put("name", "Daniel");
		document.put("age", 30);
		document.put("telefone", telefones);
		document.put("datInc", new Date());
		collection.insert(document);		
	}
	
	private static void updateUser(DB db){
		System.out.println("UPDATE");
		
		DBCollection table = db.getCollection("user");
		 
		BasicDBObject query = new BasicDBObject();
		query.put("name", "Andr�");
	 
		BasicDBObject newDocument = new BasicDBObject();
		newDocument.put("name", "Jo�o");
	 
		BasicDBObject updateObj = new BasicDBObject();
		updateObj.put("$set", newDocument);
	 
		table.update(query, updateObj);
	}
	
	private static void findUserByTelefone(DB db){
		System.out.println("FIND BY TELEFONE");
		
		DBCollection table = db.getCollection("user");
		 
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("telefone.numero", "12345678");
	 
		DBCursor cursor = table.find(searchQuery);
	 
		while (cursor.hasNext()) {
			System.out.println(cursor.next());
		}
	}
	
	private static void insertCupons(DB db){
		System.out.println("INSERT CUPONS");
		Random random = new Random();
		
		DBCollection collection = db.getCollection("cupons");
		int max = 2000;
		for(int i = 0;i<max;i++){
			BasicDBObject document = new BasicDBObject();
			
			document.put("numero", random.nextInt(800000));
			document.put("codParceiro", random.nextInt(3));
			document.put("index", i);
			
			collection.insert(document);
		}
		System.out.println( max + " CUPONS INSERIDOS");
	}

	private static void aggregate(DB db){
		/*db.cupons.aggregate( 
				{ $group : { 
						_id : "$codParceiro",
						count : {
							$sum : 1  } 
						} 
				}
			);*/
		System.out.println("AGGREGATE CUPONS");

		DBCollection collection = db.getCollection("cupons");

		BasicDBObject count = new BasicDBObject();
		count.put("_id", "$codParceiro");
		count.append("count", new BasicDBObject("$sum", 1));
		
		BasicDBObject group = new BasicDBObject();
		group.put("$group", count);
		
		List<DBObject> pipeline = new ArrayList<DBObject>();
		pipeline.add(group);
		
		AggregationOutput output = collection.aggregate(pipeline);
		for(DBObject dbObject : output.results()){
			System.out.println(dbObject);
		}
	}
	
	
}
